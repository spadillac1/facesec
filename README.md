# Facesec
Facesec is a simple app to manage the people who knock on the door.

To first run:
```
  git clone https://spadillac1@bitbucket.org/spadillac1/facesec.git
  cd facesec
  pipenv install
  cd /src/gui/
  npm install
  npm run build 
  cd ../../
  python run
```
You need to install *pipenv* and a *chromium based browser*

To development interface
```
cd facesec/src/gui/
npm run serve
```
To update interface in desktop project
```
npm run build 
```
