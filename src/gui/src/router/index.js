import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "login"
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: () => import("../views/Dashboard/Dashboard.vue"),
    children: [
      {
        path: "main",
        name: "main",
        component: () => import("../views/Dashboard/Main.vue")
      },

      {
        path: "settings",
        name: "settings",
        component: () => import("../views/Dashboard/Settings.vue")
      },
      {
        path: "registry",
        name: "registry",
        component: () => import("../views/Dashboard/Registry.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
