from sys import path
path.append("./analizador")
import reconocamip.py

@eel.expose
def alert_py():
  eel.faceDetected('sam')

def __init__():
  eel.init('./gui/dist/')
  eel.start('', size = (1020, 540), mode='chrome', port=8686)

if __name__ == "__main__":
  __init__()
